# Samara IT Community RoboAdmin

![Rust CI](https://github.com/SamaraITCommunity/admin/workflows/Rust%20CI/badge.svg)

## Domain

### Predlojka

### Adminka

## Functionality

### Predlojka 3.0

* Periodic polling of Typeform responses
* Notifications to Moderators about new Posts
* Ability to Accept Posts
* TODO: Ability to Edit/Decline Posts
* TODO: Posts publications scheduling capabilities

## Usage Guide

1. Fill in environment variables
1. Run Robot

### Environment Variables

#### Telegram

* `TELEGRAM_BOT_TOKEN` - Token from @BotFather;
* `TELEGRAM_CHANNEL_ID` - Link to the channel without `https://tg.me/` part or ID (with @ or without).

#### Discord

* `DISCORD_API_KEY` - Discord-bot token. [How to obtain](https://discordapp.com/developers/applications/).
Invite bot to the server via link: `https://discordapp.com/oauth2/authorize?client_id={BOT_ID}&scope=bot&permissions=452672`
* `DISCORD_CHANNEL_ID` - Enable developers mode in Discord, right click on a channel, Copy ID.

#### Gitlab

* `GITLAB_BRANCH` - Target branch to commit news to. 
`test.env` uses non-deployable branch, while `.env` should target deployable branch.
* `GITLAB_PERSONAL_TOKEN` - [Personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). 
* `GITLAB_PROJECT_ID` - Your's project identification. Can be found in Project->Settings->General

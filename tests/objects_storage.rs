use std::env;

use admin::objects_storage::*;

#[test]
#[ignore]
fn get_object() {
    dotenv::from_filename("test.env").ok();
    let region_env = env::var("OCI_REGION").expect("Set OCI_REGION environment variable");
    let namespace_env = env::var("OCI_NAMESPACE").expect("Set OCI_NAMESPACE environment variable");
    let bucket_name_env =
        env::var("OCI_BUCKET_NAME").expect("Set OCI_BUCKET_NAME environment variable");
    let oci_pre_authenticated_url = env::var("OCI_PRE_AUTHENTICATED_REQUEST_OBJECTS_PUT")
        .expect("Set OCI_PRE_AUTHENTICATED_REQUEST_OBJECTS_PUT environment variable");
    let bucket = Bucket::new(
        region_env,
        namespace_env,
        bucket_name_env,
        oci_pre_authenticated_url,
    );
    bucket.get("external-content.duckduckgo.com.jpg").unwrap();
}

#[test]
#[ignore]
fn put_object() {
    dotenv::from_filename("test.env").ok();
    let region_env = env::var("OCI_REGION").expect("Set OCI_REGION environment variable");
    let namespace_env = env::var("OCI_NAMESPACE").expect("Set OCI_NAMESPACE environment variable");
    let bucket_name_env =
        env::var("OCI_BUCKET_NAME").expect("Set OCI_BUCKET_NAME environment variable");
    let oci_pre_authenticated_url = env::var("OCI_PRE_AUTHENTICATED_REQUEST_OBJECTS_PUT")
        .expect("Set OCI_PRE_AUTHENTICATED_REQUEST_OBJECTS_PUT environment variable");
    let bucket = Bucket::new(
        region_env,
        namespace_env,
        bucket_name_env,
        oci_pre_authenticated_url,
    );
    let object = Object {
        name: "adminka/test".to_string(),
        data: vec![1, 2, 3],
    };
    bucket.put_preauthenticated(object).unwrap();
}

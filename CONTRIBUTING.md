## Modules Structure

* std imports
* 3rd party imports
* crate imports

* pub traits
* pub structs/enums
* impl
* impl traits
* structs/enums

* test structs
* test impls
* tests

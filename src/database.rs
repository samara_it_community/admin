use sqlx::sqlite::SqlitePool;

pub mod prelude {
    pub use sqlx::sqlite::SqlitePool as Pool;
    pub use sqlx::sqlite::SqliteRow as Row;
    pub use sqlx::{FromRow, Row as RowT};
}

pub struct Database {
    pub pool: SqlitePool,
}

impl Database {
    pub fn new(pool: SqlitePool) -> Self {
        Database { pool }
    }
}

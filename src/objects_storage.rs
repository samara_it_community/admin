use std::io::Read;

use eyre::Result;
use isahc::{prelude::*, Request};

type Url = String;
type Region = String;
type Namespace = String;
type BucketName = String;
type ObjectName = String;

pub struct Bucket {
    region: Region,
    namespace: Namespace,
    name: BucketName,
    preauthenticated_url: Url,
}

pub struct Object {
    pub name: ObjectName,
    pub data: Vec<u8>,
}

impl Bucket {
    pub fn new(
        region: impl Into<Region>,
        namespace: impl Into<Namespace>,
        bucket_name: impl Into<BucketName>,
        url: impl Into<Url>,
    ) -> Self {
        Bucket {
            region: region.into(),
            namespace: namespace.into(),
            name: bucket_name.into(),
            preauthenticated_url: url.into(),
        }
    }

    // GET /n/<namespace_string>/b/<bucket_name>/o/dummy_object HTTP/1.1
    // Host: objectstorage.us-phoenix-1.oraclecloud.com
    // connection: keep-alive
    // accept-encoding: gzip, deflate
    // accept: */*
    // user-agent: python-requests/2.10.0
    // date: Fri, 19 Aug 2016 23:43:49 GMT
    // authorization: <authorization and other headers>
    pub fn get(&self, object_name: impl Into<ObjectName>) -> Result<Object> {
        let object_name: ObjectName = object_name.into();
        let mut vector = Vec::new();
        isahc::get(format!(
            "https://objectstorage.{}.oraclecloud.com/n/{}/b/{}/o/{}",
            self.region, self.namespace, self.name, object_name
        ))?
        .body_mut()
        .read_to_end(&mut vector)?;
        Ok(Object {
            name: object_name,
            data: vector,
        })
    }

    // PUT /n/<namespace_string>/b/<bucket_name>/o/example_object HTTP/1.1
    // Host: objectstorage.us-phoenix-1.oraclecloud.com
    // Connection: keep-alive
    // Accept-Encoding: gzip, deflate
    // Accept: */*
    // User-Agent: python-requests/2.11.0
    // date: Fri, 12 Aug 2016 22:55:34 GMT
    // Content-Length: 128
    // authorization: <authorization string>
    //
    pub fn put_preauthenticated(&self, object: Object) -> Result<String> {
        let mut response = Request::put(format!("{}{}", self.preauthenticated_url, &object.name))
            .body(object.data)?
            .send()?;
        dbg!(response.text()?);

        Ok(format!(
            "https://objectstorage.{}.oraclecloud.com/n/{}/b/{}/o/{}",
            self.region, self.namespace, self.name, object.name
        ))
    }
}

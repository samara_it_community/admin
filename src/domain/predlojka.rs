//! Predlojka is a way to automate NewsFeed generation by providing users with an ability
//!to submit interesting [`Post`]s on their own with a minimum moderation from SITC
//!Administrators.

use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::fmt;
use std::str::FromStr;

use derive_more::Display;
use eyre::{Error, Result, WrapErr};
use serde::Serialize;

use crate::database::prelude::*;

/// Services that are able to publish [`Post`]s should implement this trait.
pub trait Publish {
    fn publish(&self, post: &Post) -> Result<()>;
}

#[derive(PartialEq, Eq, Debug, Display, Serialize)]
#[display(fmt = "{{{}, {:?}, {:?}, {:?}}}", meta, image, text, url)]
pub struct Post {
    pub meta: Meta,
    pub image: Option<String>,
    pub text: Option<String>,
    pub url: Option<String>,
    pub start_date: Option<String>,
    pub end_date: Option<String>,
    pub phone: Option<String>,
    pub email: Option<String>,
    pub contact: Option<String>,
}

impl Post {
    pub fn as_md(&self) -> String {
        format!(
            include_str!("../resources/publish/templates/post.md"),
            self.meta.title,
            self.start_date.as_ref().unwrap_or(&"".to_string()),
            self.end_date.as_ref().unwrap_or(&"".to_string()),
            self.text.as_ref().unwrap_or(&"".to_string()),
            self.meta.author,
            self.phone.as_ref().unwrap_or(&"".to_string()),
            self.email.as_ref().unwrap_or(&"".to_string()),
            self.contact.as_ref().unwrap_or(&"".to_string()),
            self.meta.tags
        )
    }
}

impl<'r> FromRow<'r, Row> for Post {
    fn from_row(row: &'r Row) -> Result<Self, sqlx::Error> {
        Ok(Post {
            meta: Meta {
                id: row.try_get("id")?,
                token: row.try_get("external_id")?,
                date_time: row.try_get("date_time")?,
                author: row.try_get("author")?,
                title: row.try_get("title")?,
                tags: Tags::from_str(&row.try_get::<String, _>("tags")?).unwrap(),
            },
            image: row.try_get("image")?,
            text: row.try_get("text")?,
            url: row.try_get("url")?,
            start_date: row.try_get("start_date")?,
            end_date: row.try_get("end_date")?,
            phone: row.try_get("phone")?,
            email: row.try_get("email")?,
            contact: row.try_get("contact")?,
        })
    }
}

#[derive(Eq, Debug, Display, Serialize)]
#[display(
    fmt = "[{};{};{};{};{};{:?}]",
    id,
    token,
    date_time,
    author,
    title,
    tags
)]
pub struct Meta {
    pub id: i32,
    pub token: String,
    pub date_time: String,
    pub author: String,
    pub title: String,
    pub tags: Tags,
}

#[derive(PartialEq, Eq, Debug, Serialize)]
pub struct Tags(Vec<Tag>);

impl Tags {
    pub fn single(tag: Tag) -> Self {
        Tags(vec![tag])
    }
}

impl fmt::Display for Tags {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            self.0.iter().fold("".to_string(), |accum, item| {
                dbg!(&item);
                format!("{} {}", accum, item)
            }),
        )
    }
}

impl FromStr for Tags {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        dbg!(s);
        Ok(Tags(
            s.trim()
                .split(' ')
                .map(Tag::from_str)
                .collect::<Result<Vec<Tag>>>()?,
        ))
    }
}

/// Possible tags for [`Post`].
///
/// Representations in social networks:
/// * `#мероприятия`
/// * `#вакансии`
/// * `#обучение`
/// * `#полезное`
/// * `#резюме`
/// * `#другое`
#[derive(PartialEq, Eq, Debug, Display, Serialize)]
#[display(fmt = "#{}")]
pub enum Tag {
    #[display(fmt = "мероприятия")]
    Announcement,
    #[display(fmt = "вакансии")]
    Job,
    #[display(fmt = "обучение")]
    Education,
    #[display(fmt = "полезное")]
    Information,
    #[display(fmt = "резюме")]
    Cv,
    #[display(fmt = "другое")]
    Another,
}

impl FromStr for Tag {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        use Tag::*;
        dbg!(s);
        Ok(match s.trim() {
            "#мероприятия" => Announcement,
            "#вакансии" => Job,
            "#обучение" => Education,
            "#полезное" => Information,
            "#резюме" => Cv,
            _ => Another,
        })
    }
}

impl Ord for Post {
    fn cmp(&self, other: &Self) -> Ordering {
        self.meta.cmp(&other.meta)
    }
}

impl PartialOrd for Post {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Option::Some(self.cmp(other))
    }
}

impl Ord for Meta {
    fn cmp(&self, other: &Self) -> Ordering {
        self.token.cmp(&other.token)
    }
}

impl PartialOrd for Meta {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Option::Some(self.cmp(other))
    }
}

impl PartialEq for Meta {
    fn eq(&self, other: &Self) -> bool {
        self.token == other.token
    }
}

//TODO: move under test feature flag?
impl Post {
    pub fn generate() -> Self {
        Post {
            meta: Meta {
                id: 1,
                author: "noname".to_string(),
                token: "ASLKJDHQU@HE@!*HD!U@HDLH".to_string(),
                date_time: "now".to_string(),
                title: "generated post's title".to_string(),
                tags: Tags(vec![Tag::Job]),
            },
            image: None,
            text: Some("generated post's content".to_string()),
            url: None,
            start_date: None,
            end_date: None,
            phone: None,
            email: None,
            contact: None,
        }
    }

    pub fn generate_event() -> Self {
        Post {
            meta: Meta {
                id: 1,
                author: "noname".to_string(),
                token: "ASLKJDHQU@HE@!*HD!U@HDLH".to_string(),
                date_time: "now".to_string(),
                title: "generated post's title".to_string(),
                tags: Tags(vec![Tag::Job]),
            },
            image: None,
            text: Some("generated post's content".to_string()),
            url: None,
            start_date: Some("2025-02-20T00:00:00.000Z".to_string()),
            end_date: None,
            phone: None,
            email: None,
            contact: None,
        }
    }

    pub async fn archive(&self, pool: &Pool) -> Result<()> {
        sqlx::query!(
            r#"INSERT INTO published_posts (external_id, date_time, title, author, tags, image, text, url)
                SELECT external_id, date_time, title, author, tags, image, text, url
                FROM posts
                WHERE external_id = ?1"#,
                self.meta.token,
                )
            .execute(pool)
            .await
            .wrap_err("failed to archive published post")?;
        sqlx::query!(
            r#"DELETE FROM posts
                WHERE external_id = ?1"#,
            self.meta.token,
        )
        .execute(pool)
        .await
        .wrap_err("failed to clean up published post")?;
        Ok(())
    }
}

pub struct Posts;

impl Posts {
    pub async fn first(pool: &Pool) -> Result<Option<Post>> {
        sqlx::query_as("SELECT * FROM posts ORDER BY date_time ASC")
            .fetch_optional(pool)
            .await
            .wrap_err("failed to select oldest post from database")
    }

    pub async fn last_post_external_id(pool: &Pool) -> Result<Option<String>> {
        let last_published_post_external_id =
            sqlx::query_scalar("SELECT external_id FROM published_posts ORDER BY date_time DESC")
                .fetch_optional(pool)
                .await
                .wrap_err("failed to select last post id from database")?;
        let last_post_external_id =
            sqlx::query_scalar("SELECT external_id FROM posts ORDER BY date_time DESC")
                .fetch_optional(pool)
                .await
                .wrap_err("failed to select last post id from database")?
                .or(last_published_post_external_id);
        dbg!(&last_post_external_id);
        Ok(last_post_external_id)
    }

    pub async fn append(pool: &Pool, new_posts: BinaryHeap<Post>) -> Result<()> {
        dbg!(&new_posts);
        for post in new_posts {
            let tags = post.meta.tags.to_string();
            sqlx::query!(
                r#"INSERT INTO posts
                    (external_id, date_time, title, author, tags, image, text, url)
                    VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)"#,
                post.meta.token,
                post.meta.date_time,
                post.meta.title,
                post.meta.author,
                tags,
                post.image,
                post.text,
                post.url,
            )
            .execute(pool)
            .await
            .wrap_err("failed to insert new post")?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_publish_post_to_stdout() {
        struct Stdout;
        impl Publish for Stdout {
            fn publish(&self, post: &Post) -> Result<()> {
                println!("{}", post);
                Ok(())
            }
        }
        Stdout {}.publish(&Post::generate()).unwrap();
    }
}

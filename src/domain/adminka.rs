use std::str::FromStr;

use eyre::{eyre, Error, Result};

/// Commands are used to declaratively tell our Bot what to do.
///
/// Implementation details will be decided by Bot based on command, state and configuration.
#[derive(Debug, Clone)]
pub enum Command {
    NotifyAdministrators(Notification),
    VoteForBan {
        reason: Reason,
        user_to_ban_id: Id,
        user_who_vote_id: Id,
    },
    Post(PostCommand),
}

#[derive(Debug, Clone)]
pub enum Reason {
    Spam,
    RulesViolation(Rules),
}

impl FromStr for Reason {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s.to_lowercase().as_str() {
            "spam" | "спам" => Reason::Spam,
            _ => {
                if let Some(rule) = s.strip_prefix("rule#") {
                    Reason::RulesViolation(Rules::from_str(rule)?)
                } else {
                    return Err(eyre!("failed to parse a Reason from str: {}", s));
                }
            }
        })
    }
}

#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum Rules {
    Aggression,
    Stickers,
    BadLanguage,
}

impl FromStr for Rules {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s.to_lowercase().as_str() {
            "aggression" | "аггрессия" => Rules::Aggression,
            "stickers" | "стикеры" => Rules::Stickers,
            "bad_language" | "мат" => Rules::BadLanguage,
            _ => return Err(eyre!("failed to parse a Rules from str: {}", s)),
        })
    }
}

type Notification = String;
type Id = String;

#[derive(Debug, Clone)]
pub enum PostCommand {
    Show,
    Publish,
    Decline,
}

impl FromStr for PostCommand {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self> {
        use PostCommand::*;
        match s {
            "show" => Ok(Show),
            "publish" => Ok(Publish),
            "decline" => Ok(Decline),
            _ => Err(eyre!("Unsupported PostCommand variant: {}", s)),
        }
    }
}

use std::{env, sync::mpsc, thread};

use eyre::Result;
use sqlx::sqlite::SqlitePoolOptions as PoolOptions;
use std::time::Duration;
use tokio::{runtime::Runtime, time};

use typeform_rs::Typeform;

use admin::{
    database::{prelude::*, Database},
    objects_storage::Bucket,
    services::{
        gitlab::Gitlab,
        social_networks::{
            discord::Discord,
            telegram::{Telegram, TelegramAdmin},
        },
        typeform,
    },
    Bot,
};

fn main() -> Result<()> {
    dotenv::dotenv().ok();
    let rt = Runtime::new().unwrap();

    let pool: Pool = rt.block_on(async {
        let pool = PoolOptions::new()
            .max_connections(5)
            .connect(env::var("DATABASE_URL").unwrap().as_str())
            .await
            .unwrap();
        sqlx::migrate!().run(&pool).await.unwrap();
        pool
    });
    let region_env = env::var("OCI_REGION").expect("Set OCI_REGION environment variable");
    let namespace_env = env::var("OCI_NAMESPACE").expect("Set OCI_NAMESPACE environment variable");
    let bucket_name_env =
        env::var("OCI_BUCKET_NAME").expect("Set OCI_BUCKET_NAME environment variable");
    let oci_pre_authenticated_url = env::var("OCI_PRE_AUTHENTICATED_REQUEST_OBJECTS_PUT")
        .expect("Set OCI_PRE_AUTHENTICATED_REQUEST_OBJECTS_PUT environment variable");
    let bucket = Bucket::new(
        region_env,
        namespace_env,
        bucket_name_env,
        oci_pre_authenticated_url,
    );
    let gitlab = rt.block_on(async {
        Gitlab::new(
            env::var("GITLAB_PERSONAL_TOKEN")
                .expect("Please set GITLAB_PERSONAL_TOKEN env variable"),
            env::var("GITLAB_PROJECT_ID")
                .expect("Please set GITLAB_PROJECT_ID env variable")
                .parse()
                .expect("Failed to parse GITLAB_PROJECT_ID"),
            env::var("GITLAB_BRANCH").expect("Please set GITLAB_BRANCH env variable"),
        )
        .await
        .unwrap()
    });
    let bot = Bot {
        database: Database::new(pool),
        objects_storage: bucket,
        gitlab,
        discord: Discord::new(
            env::var("DISCORD_API_KEY").expect("Set DISCORD_API_KEY environment variable"),
            env::var("DISCORD_CHANNEL_ID")
                .expect("Set DISCORD_CHANNEL_ID environment variable")
                .parse()
                .expect("DISCORD_CHANNEL_ID should be integer"),
        )?,
        telegram: Telegram::new(
            env::var("TELEGRAM_BOT_TOKEN").expect("Set TELEGRAM_BOT_TOKEN environment variable"),
            env::var("TELEGRAM_CHANNEL_ID").expect("Set TELEGRAM_CHANNEL_ID environment variable"),
            env::var("TELEGRAM_CHAT_ID").expect("Set TELEGRAM_CHAT_ID environment variable"),
        ),
        typeform: Typeform::new(
            &env::var(typeform::TYPEFORM_FORM_ID)
                .expect("Failed to get TYPEFORM_FORM_ID environment variable."),
            &env::var(typeform::TYPEFORM_ACCESS_TOKEN)
                .expect("Failed to get TYPEFORM_ACCESS_TOKEN environment variable."),
        ),
    };
    let (tx, rx) = mpsc::sync_channel(100);
    let admin_handler = thread::spawn(move || {
        let rt = Runtime::new().unwrap();
        dbg!("Starting admin");
        rt.block_on(async {
            let telegram_admin = TelegramAdmin::new(
                env::var("TELEGRAM_BOT_TOKEN")
                    .expect("Set TELEGRAM_BOT_TOKEN environment variable"),
                env::var("TELEGRAM_CHAT_ID").expect("Set TELEGRAM_CHAT_ID environment variable"),
                tx.clone(),
            );
            telegram_admin.poll_handlers().await
        });
    });
    let bot_handler = thread::spawn(move || {
        let rt = Runtime::new().unwrap();
        dbg!("Poll posts");
        rt.block_on(async {
            loop {
                if let Err(error) = bot.poll_posts().await {
                    eprintln!("failed to poll posts: {:?}", error);
                } else {
                    match rx.try_recv() {
                        Ok(command) => {
                            dbg!("New command: {:?}", &command);
                            bot.handle_command(&command).await.unwrap();
                        }
                        Err(error) => eprintln!("failed to recv command: {:?}", error),
                    }
                    time::sleep(Duration::from_secs(30)).await;
                }
            }
        });
    });
    bot_handler.join().unwrap();
    admin_handler.join().unwrap();
    Ok(())
}

use std::{convert::TryFrom, str::FromStr, sync::mpsc::SyncSender as Sender};

use carapax::{
    handler,
    longpoll::LongPoll,
    methods::{GetChatAdministrators, SendMessage, SendPhoto},
    types::{Command, InputFile},
    Api, Config, Dispatcher, ExecuteError,
};
use eyre::{eyre, Error, Result, WrapErr};

use crate::{
    domain::adminka::Command as AdminkaCommand,
    domain::{
        adminka::{PostCommand, Reason},
        predlojka::Post,
    },
};

pub struct Telegram {
    api: Api,
    channel_id: String,
    chat_id: String,
}

impl Telegram {
    pub fn new(token: String, channel_id: String, chat_id: String) -> Self {
        let config = Config::new(token);
        Telegram {
            api: Api::new(config).expect("Failed to create API"),
            channel_id,
            chat_id,
        }
    }

    pub async fn publish(&self, post: &Post) -> Result<()> {
        if let Some(image_url) = &post.image {
            if let Err(error) = self
                .api
                .execute(SendPhoto::new(
                    self.channel_id.clone(),
                    InputFile::url(image_url),
                ))
                .await
                .wrap_err("Failed to send telegram photo.")
            {
                eprintln!("{:?}", error);
            }
        }
        if let Err(error) = self
            .api
            .execute(
                SendMessage::new(self.channel_id.clone(), post.as_md())
                    .disable_web_page_preview(true),
            )
            .await
            .wrap_err("Failed to send telegram message.")
        {
            eprintln!("{:?}", error);
        }
        if let Some(url) = &post.url {
            if let Err(error) = self
                .api
                .execute(SendMessage::new(self.channel_id.clone(), url))
                .await
                .wrap_err("Failed to send telegram url.")
            {
                eprintln!("{:?}", error);
            }
        }
        Ok(())
    }

    pub async fn send_to_administrators(&self, notification: &str) -> Result<()> {
        self.api
            .execute(SendMessage::new(
                self.chat_id.clone(),
                format!(
                    "{}\n{:#?}",
                    notification,
                    self.api
                        .execute(GetChatAdministrators::new(self.chat_id.clone()))
                        .await?
                        .iter()
                        .filter_map(|chat_member| {
                            chat_member
                                .get_user()
                                .username
                                .as_ref()
                                .map(|username| format!("@{}", username))
                        })
                        .collect::<Vec<_>>(),
                ),
            ))
            .await
            .map(|_| ())
            .wrap_err("Failed to notify_administrators.")
    }

    //TODO: propagate initial chat id and user for p2p or user facing commands
    pub async fn answer(&self, answer: &str) -> Result<()> {
        self.api
            .execute(SendMessage::new(self.chat_id.clone(), answer))
            .await
            .map(|_| ())
            .wrap_err("Failed to answer.")
    }
}

pub struct TelegramAdmin {
    api: Api,
    commands_sender: Sender<AdminkaCommand>,
    chat_id: String,
}

impl TelegramAdmin {
    pub fn new(token: String, chat_id: String, commands_sender: Sender<AdminkaCommand>) -> Self {
        let config = Config::new(token);
        TelegramAdmin {
            api: Api::new(config).expect("Failed to create API"),
            commands_sender,
            chat_id,
        }
    }

    pub async fn poll_handlers(self) {
        dbg!("poll handlers start.");
        let api = self.api.clone();
        let mut dispatcher = Dispatcher::new(self);
        dispatcher.add_handler(handle_any);
        LongPoll::new(api, dispatcher).run().await;
        dbg!("poll handlers end.");
    }
}

#[handler]
async fn handle_any(context: &TelegramAdmin, command: Command) -> Result<(), ExecuteError> {
    let name = command.get_name();
    dbg!(&name);
    let message = command.get_message();
    let chat_id = message.get_chat_id();

    if let Some(user) = message.get_user() {
        if context
            .api
            .execute(GetChatAdministrators::new(context.chat_id.clone()))
            .await?
            .iter()
            .map(|chat_member| chat_member.get_user().id)
            .any(|x| x == user.id)
        {
            match AdminkaCommand::try_from(&command) {
                Ok(adminka_command) => {
                    context
                        .api
                        .execute(SendMessage::new(
                            chat_id,
                            format!(
                                include_str!("../../resources/messages/command_acceptance.txt"),
                                name
                            ),
                        ))
                        .await?;
                    if let Err(error) = context.commands_sender.send(adminka_command) {
                        eprintln!("failed to send AdminkaCommand: {}", error);
                    }
                }
                Err(error) => {
                    context
                        .api
                        .execute(SendMessage::new(
                            chat_id,
                            format!(
                                include_str!("../../resources/messages/command_reject.txt"),
                                name, error
                            ),
                        ))
                        .await?;
                }
            }
        } else {
            let error_msg = format!("{} user is not an administrator", user.id);
            context
                .api
                .execute(SendMessage::new(
                    chat_id,
                    format!(
                        include_str!("../../resources/messages/command_reject.txt"),
                        name, error_msg
                    ),
                ))
                .await?;
        }
    }
    Ok(())
}

impl TryFrom<&Command> for AdminkaCommand {
    type Error = Error;

    fn try_from(command: &Command) -> Result<Self> {
        Ok(match command.get_name() {
            "/help" => AdminkaCommand::NotifyAdministrators(
                "`post show` - posmotreti predlojku, `post publish` - opublikovati novost"
                    .to_string(),
            ),
            "/post" => AdminkaCommand::Post(PostCommand::from_str(
                command
                    .get_args()
                    .get(0)
                    .ok_or_else(|| eyre!("action wasn't specified"))?
                    .to_lowercase()
                    .as_str(),
            )?),
            "/ban" => AdminkaCommand::VoteForBan {
                reason: Reason::from_str(
                    command
                        .get_args()
                        .get(0)
                        .ok_or_else(|| eyre!("reason wasn't specified"))?
                        .as_str(),
                )?,
                user_to_ban_id: command
                    .get_args()
                    .get(1)
                    .ok_or_else(|| eyre!("user wasn't specified"))?
                    .clone(),
                user_who_vote_id: command.get_message().get_user().unwrap().id.to_string(),
            },
            _ => {
                return Err(eyre!(
                    "failed to convert Command: {:?} to an AdminkaCommand",
                    &command
                ))
            }
        })
    }
}

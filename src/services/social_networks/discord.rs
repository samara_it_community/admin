use discord::model::ChannelId;
use discord::Discord as Origin;
use eyre::{Result, WrapErr};

use crate::domain::predlojka::{Post, Publish};

pub struct Discord {
    origin: Origin,
    channel_id: u64,
}

impl Discord {
    pub fn new(token: String, channel_id: u64) -> Result<Self> {
        Ok(Discord {
            origin: Origin::from_bot_token(&token).wrap_err("failed to initialize discord")?,
            channel_id,
        })
    }
}

impl Publish for Discord {
    fn publish(&self, post: &Post) -> Result<()> {
        self.origin
            .send_message(ChannelId(self.channel_id), &post.as_md(), "", false)
            .map(|message| {
                println!("discord message sent: {:?}", &message);
            })
            .wrap_err("failed to send message to discord")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;

    #[test]
    fn publish_post() {
        dotenv::from_filename("test.env").expect("Failed to read env variables from test.env");
        let discord = Discord::new(
            env::var("DISCORD_API_KEY").expect("Set DISCORD_API_KEY environment variable"),
            env::var("DISCORD_CHANNEL_ID")
                .expect("Set DISCORD_CHANNEL_ID environment variable")
                .parse()
                .expect("DISCORD_CHANNEL_ID should be integer"),
        )
        .expect("Failed to build discord");
        let post = Post::generate();
        discord
            .publish(&post)
            .expect("Failed to publish Discord post");
    }
}

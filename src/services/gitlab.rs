use eyre::{Result, WrapErr};
use gitlab::{
    self,
    api::{self, common::NameOrId, projects::repository::files::CreateFile, AsyncQuery},
};

use crate::domain::predlojka::Post;

pub struct Gitlab {
    origin: gitlab::AsyncGitlab,
    project_id: gitlab::ProjectId,
    target_branch: String,
}

impl Gitlab {
    pub async fn new(token: String, project_id: u64, target_branch: String) -> Result<Self> {
        Ok(Gitlab {
            origin: gitlab::GitlabBuilder::new("gitlab.com", token)
                .build_async()
                .await
                .wrap_err("failed to create gitlab client")?,
            project_id: gitlab::ProjectId::new(project_id),
            target_branch,
        })
    }

    pub async fn publish(&self, post: &Post) -> Result<()> {
        api::ignore(CreateFile::builder()
            .project(NameOrId::from(self.project_id.value()))
            .file_path(format!(
                "content/{}{}.md",
                post.meta.date_time,
                post.meta.title.len()
            ))
            .branch(self.target_branch.clone())
            .commit_message(format!("New Post - {}", post.meta.title))
            .content(
                format!(
                    "+++\n title = \"{}\"\n date = {}\n description = \"{}\"\n [extra]\n preview_img =\"{}\"\n tags =\"#{}\"\n+++\n{}",
                    post.meta.title,
                    post.meta.date_time,
                    urlencoding::encode(&post.meta.title),
                    post.image.as_ref().unwrap_or(&"https://sun9-25.userapi.com/impf/bJwX5W1HKBqLhy9gYtqNGFNK3Xbz7pjWaK1OUw/nB1UgrClTh0.jpg?size=795x265&quality=95&crop=0,0,1590,530&sign=92df3480e634bf6a2670c5f1326b6822&type=cover_group".to_string()),
                    post.meta.tags,
                    &post.as_md()
                ).as_bytes())
            .build()
            .wrap_err("failed to build create file command")?)
            .query_async(&self.origin).await
            .wrap_err("failed to execute create file query")?;
        Ok(())
    }
}

use std::{convert::TryInto, path::PathBuf};

use eyre::{eyre, Error, Result};
use ics::{
    properties::{Description, DtEnd, DtStart, Summary},
    Event as IEvent, ICalendar,
};
use uuid::Uuid;

use crate::domain::predlojka::{Post, Publish};

struct Calendar {
    file: PathBuf,
}

impl Publish for Calendar {
    fn publish(&self, post: &Post) -> Result<()> {
        let mut calendar = ICalendar::new("2.0", "ics-rs");
        let event: IEvent = post.try_into()?;
        calendar.add_event(event);
        calendar.save_file(self.file.clone())?;
        Ok(())
    }
}

impl<'a> TryInto<IEvent<'a>> for &'a Post {
    type Error = Error;

    fn try_into(self) -> Result<IEvent<'a>, Self::Error> {
        if let (Some(text), Some(start_date_time)) = (&self.text, &self.start_date) {
            let mut event = IEvent::new(Uuid::new_v4().to_string(), start_date_time.to_string());
            event.push(Summary::new(self.meta.title.clone()));
            event.push(Description::new(ics::escape_text(text)));
            event.push(DtStart::new(start_date_time.to_string()));
            if let Some(end_date_time) = &self.end_date {
                event.push(DtEnd::new(end_date_time.to_string()));
            }
            Ok(event)
        } else {
            Err(eyre!("not an event"))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn post_to_calendar_event() {
        let temp_file = temp_file::empty();
        Calendar {
            file: temp_file.path().to_path_buf(),
        }
        .publish(&Post::generate_event())
        .unwrap();
    }
}

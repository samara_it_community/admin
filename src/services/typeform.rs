use std::convert::TryFrom;

use eyre::{eyre, Error, Result};

use typeform_rs::{Answer, Response};

use crate::domain::predlojka::{Meta, Post, Tag, Tags};

pub const TYPEFORM_FORM_ID: &str = "TYPEFORM_FORM_ID";
pub const TYPEFORM_ACCESS_TOKEN: &str = "TYPEFORM_ACCESS_TOKEN";

impl TryFrom<Response> for Post {
    type Error = Error;

    fn try_from(response: Response) -> Result<Post> {
        let answers = response.answers.ok_or(eyre!("No answers in response."))?;
        let text = answers
            .find(references::TEXT)
            .and_then(Answer::as_str)
            .map(|s| s.to_string());
        let url = Some(format!(
            "\n{}",
            answers
                .find(references::LINK)
                .and_then(Answer::as_str)
                .unwrap_or_default()
        ));
        let mut image = None;
        if let Some(file) = answers.find(references::IMAGE).map(Answer::as_str) {
            image = Some(file.ok_or(eyre!("Failed to parse file url."))?.to_string());
        }
        Ok(Post {
            meta: Meta {
                id: 1,
                token: response.token,
                date_time: response.landed_at,
                author: answers
                    .find(references::AUTHOR)
                    .ok_or(eyre!("No author in answers."))?
                    .as_str()
                    .ok_or(eyre!("Author is not a string."))?
                    .to_string(),
                title: answers
                    .find(references::TITLE)
                    .ok_or(eyre!("No title in answers."))?
                    .as_str()
                    .ok_or(eyre!("Title is not a string."))?
                    .to_string(),
                tags: Tags::single(
                    match answers
                        .find(references::TYPE)
                        .ok_or(eyre!("No type in answers."))?
                        .as_str()
                        .ok_or(eyre!("Type is not a string."))?
                    {
                        tags::ANNOUNCEMENT => Tag::Announcement,
                        tags::JOB => Tag::Job,
                        tags::EDUCATION => Tag::Education,
                        tags::INFORMATION => Tag::Information,
                        tags::CV => Tag::Cv,
                        _ => Tag::Another,
                    },
                ),
            },
            text,
            image,
            url,
            start_date: answers
                .find(references::START_DATE)
                .and_then(Answer::as_str)
                .map(|s| s.to_string()),
            end_date: answers
                .find(references::END_DATE)
                .and_then(Answer::as_str)
                .map(|s| s.to_string()),
            phone: answers
                .find(references::PHONE)
                .and_then(Answer::as_str)
                .map(|s| s.to_string()),
            email: answers
                .find(references::EMAIL)
                .and_then(Answer::as_str)
                .map(|s| s.to_string()),
            contact: answers
                .find(references::CONTACT)
                .and_then(Answer::as_str)
                .map(|s| s.to_string()),
        })
    }
}

mod references {
    pub const AUTHOR: &str = "01FQA2BHCPYJHJ0ZXHD0WR1JFK";
    pub const TITLE: &str = "2d48740f-37c2-42f0-b987-e960c318c983";
    pub const TEXT: &str = "af4428f4-0b2a-4a00-8a2d-1d191b49f056";
    pub const TYPE: &str = "f03a630c-2dd7-4a00-b1f9-9b802022b811";
    pub const LINK: &str = "0e62ecbc-fce2-4b23-b172-57293f5358af";
    pub const IMAGE: &str = "b98d8ed6-dadb-4886-8579-fe6a04f6a3a6";
    pub const START_DATE: &str = "627282c4-86f8-4d2b-98ed-63c4462e3939";
    pub const END_DATE: &str = "6fe9732b-89d6-4ba3-9d93-8437fc881fcc";
    pub const PHONE: &str = "1a30e567-8cdf-4e81-8a9a-eaecab811f96";
    pub const EMAIL: &str = "0a7d0661-4fe6-404d-b74d-1f50bd0fc64a";
    pub const CONTACT: &str = "6f945592-f12d-4d78-b586-b2c9726093ef";
}

pub mod fields {
    pub const IMAGE: &str = "wYuykcQCh8F3";
}

mod tags {
    pub const ANNOUNCEMENT: &str = "Анонс мероприятия";
    pub const JOB: &str = "Предложение о работе";
    pub const EDUCATION: &str = "Тренинг или обучение";
    pub const INFORMATION: &str = "Полезная информация";
    pub const CV: &str = "Резюме";
    //pub const ANOTHER: &str = "Другое";
}

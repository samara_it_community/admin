pub mod database;
pub mod domain;
pub mod objects_storage;
pub mod services;

use std::collections::BinaryHeap;
use std::convert::TryFrom;

use eyre::Result;
use typeform_rs::Typeform;

use crate::domain::adminka::Command;

use self::database::Database;
use self::domain::adminka::PostCommand;
use self::domain::predlojka::{Post, Posts, Publish};
use self::objects_storage::{Bucket, Object};
use self::services::gitlab::Gitlab;
use self::services::social_networks::discord::Discord;
use self::services::social_networks::telegram::Telegram;
use self::services::typeform::fields;

pub struct Bot {
    pub database: Database,
    pub discord: Discord,
    pub telegram: Telegram,
    pub typeform: Typeform,
    pub gitlab: Gitlab,
    pub objects_storage: Bucket,
}

impl Bot {
    pub async fn poll_posts(&self) -> Result<()> {
        let posts: BinaryHeap<Post> =
            if let Some(last_post_id) = Posts::last_post_external_id(&self.database.pool).await? {
                self.typeform.responses_after(&last_post_id)?
            } else {
                self.typeform.responses()?
            }
            .items
            .iter()
            .filter_map(|response| {
                let mut maybe_post = Post::try_from(response.clone()).ok();
                if let Some(post) = &mut maybe_post {
                    if let Some(typeform_image_url) = &post.image {
                        let image_name = typeform_image_url.rsplit_once('/').unwrap().1;
                        let file = self
                            .typeform
                            .response_file(
                                post.meta.token.clone(),
                                fields::IMAGE.to_string(),
                                image_name.to_string(),
                            )
                            .unwrap();
                        let name = format!(
                            "adminka/{}{}",
                            post.meta.token,
                            urlencoding::encode(image_name)
                        );
                        post.image = Some(
                            self.objects_storage
                                .put_preauthenticated(Object { name, data: file })
                                .unwrap(),
                        );
                    }
                }
                maybe_post
            })
            .collect();
        if !posts.is_empty() {
            Posts::append(&self.database.pool, posts).await?;
            self.handle_command(&Command::NotifyAdministrators(
                include_str!("resources/messages/notify_administrators/new_posts.md").to_string(),
            ))
            .await?;
        }
        Ok(())
    }

    pub async fn handle_command(&self, command: &Command) -> Result<()> {
        if let Command::Post(post_command) = command {
            let answer = match post_command {
                PostCommand::Show => format!("{:?}", Posts::first(&self.database.pool).await?),
                PostCommand::Publish => "Going to publish post.".to_string(),
                PostCommand::Decline => {
                    if let Some(post) = Posts::first(&self.database.pool).await? {
                        post.archive(&self.database.pool).await?;
                        format!("Post {:?} declined.", &post.meta.title)
                    } else {
                        "No post post to decline.".to_string()
                    }
                }
            };
            self.telegram.answer(answer.as_str()).await?;
        }
        if let Command::NotifyAdministrators(notification) = command {
            self.telegram.send_to_administrators(notification).await?;
        }
        if let Command::Post(PostCommand::Publish) = command {
            if let Some(post) = Posts::first(&self.database.pool).await? {
                self.telegram.publish(&post).await?;
                self.discord.publish(&post)?;
                self.gitlab.publish(&post).await?;
                post.archive(&self.database.pool).await?;
            }
        }
        Ok(())
    }
}

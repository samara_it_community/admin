CREATE TABLE IF NOT EXISTS posts (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    external_id TEXT    NOT NULL,
    date_time   INTEGER NOT NULL,
    title       TEXT    NOT NULL,
    author      TEXT    NOT NULL,
    tags        TEXT    NOT NULL,
    image       TEXT,
    text        TEXT,
    url         TEXT,
    start_date  TEXT,
    end_date    TEXT,
    phone       TEXT,
    email       TEXT,
    contact     TEXT
);
CREATE TABLE IF NOT EXISTS published_posts (
    id          INTEGER PRIMARY KEY AUTOINCREMENT,
    external_id TEXT    NOT NULL,
    date_time   INTEGER NOT NULL,
    title       TEXT    NOT NULL,
    author      TEXT    NOT NULL,
    tags        TEXT    NOT NULL,
    image       TEXT,
    text        TEXT,
    url         TEXT,
    start_date  TEXT,
    end_date    TEXT,
    phone       TEXT,
    email       TEXT,
    contact     TEXT
);
